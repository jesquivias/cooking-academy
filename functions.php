<?php
/* Disable Gutenberg */
// add_filter('use_block_editor_for_post', '__return_false');

/* Add Widget of Theme with the next courses */
require_once dirname(__FILE__) . '/inc/widgets.php';

/* Add the Post Types of instructors and courses */
require_once dirname(__FILE__) . '/inc/posttypes.php';

/* Add CMB2 */
require_once dirname(__FILE__) . '/cmb2.php';

/* Add Queries */
require_once dirname(__FILE__) . '/inc/queries.php';

/* Load custom files of CMB2 */
require_once dirname(__FILE__) . '/inc/custom-fields.php';

/*  Options of Theme*/
require_once dirname(__FILE__) . '/inc/options.php';


/* Featured Images for Pages Image thumbnails */
add_action('init', 'edc_imagen_destacada');
function edc_imagen_destacada($id) {
	$imagen = get_the_post_thumbnail_url($id, 'full');

	$html = '';
	$clase = false;
	if($imagen) {
		$clase = true;
		$html .= '<div class="container">';
		$html .= 	 '<div class="row imagen-destacada"></div>';
		$html .= '</div>';

		// Add styles linears
		wp_register_style('custom', false);
		wp_enqueue_style('custom');

		// CSS for custom
		$imagen_destacada_css = "
			.imagen-destacada {
					background-image: url( {$imagen} );
			}
		";
		wp_add_inline_style('custom', $imagen_destacada_css);
	}
	return array($html, $clase);

}

/* Function that load when activate the theme */
function edc_setup() {
	/* Define sizes of images */
	add_image_size('mediano', 510, 340, true);
	add_image_size('cuadrada_mediana', 350, 350, true);

	/* Image thumbnails and Support title-tag */
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag');

	/* Navbars */
	register_nav_menus( array(
			'menu_principal' => esc_html__('Menu Principal', 'cookingacademy')
		) );
}
add_action('after_setup_theme', 'edc_setup');

/* Add the class nav-link of bootstrap in the menu principal */
function edc_enlace_class($atts, $item, $args) {
	if ($args->theme_location == 'menu_principal') {
		$atts['class'] = 'nav-link';
	}
	return $atts;
}
add_filter('nav_menu_link_attributes', 'edc_enlace_class', 10, 3 );


/* Charge Scripts and CSS Theme */
function edc_scripts() {
	/* Styles */
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css', false, '4.1.3');
	wp_enqueue_style( 'style', get_stylesheet_uri(), array('bootstrap-css') );
	/* JS */
	wp_enqueue_script('jquery');
	wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.js', array('jquery'), '1.0', true );
	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', array('popper'), '1.0', true );
}
add_action('wp_enqueue_scripts', 'edc_scripts');


/* Add a custom message to page in the Admin of CMS */
add_filter('display_post_states', 'edc_cambiar_estado', 10, 2 );
function edc_cambiar_estado($states, $post) {
	if( ( 'page' === get_post_type($post->ID) ) && ( 'page-cursos.php' == get_page_template_slug($post->ID) ) ) {
		$states[] = __('Pagina de Cursos   <a href="post-new.php?post_type=clases_cocina">Add New Course</a>');
	}
	return $states;
}

/* Widgets Support */
add_action('widgets_init', 'edc_widgets_sidebar');
function edc_widgets_sidebar() {
	register_sidebar(array(
		'name'					=> 'Widget Lateral',
		'id'						=> 'sidebar_widget',
		'before_widget' => '<div class="widget">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h2 class="text-center text-light separador inverso">',
		'after_title'		=> '</h2>'
	));
}
